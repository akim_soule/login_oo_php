<?php

/**
 *
 */
class Autoloader {

	static function register() {
		spl_autoload_register(array(__CLASS__, 'autoload'));
	}

	static function autoload($class_name) {
		$dossiers = array(
			'Controleur/',
			'Modele/',
			'Config/'
		);

		//for each directory
		foreach ($dossiers as $dossier) {
			//see if the file exsists
			if (file_exists($dossier.$class_name.'.php')) {
				require_once $dossier.$class_name.'.php';
				//only require the class once, so quit after to save effort (if you got more, then name them something else
			}
		}
	}

}
?>