<?php

class Commentaire extends Modele {

	//Methode pour acceder aux commentaires
	public function getCommentaires($nom) {
		$sql = 'SELECT ID_COMMENTAIRE, CONTENU FROM COMMENTAIRE WHERE ID_USER = (SELECT ID_USER FROM UTILISATEUR WHERE NOM_USER = ?';
		$commantaires = $this->executerRequete($sql, array($nom));
		$commantaires = $commantaires->fetchAll();
		return $commantaires;
	}
	//Methode pour acceder aux commentaires du plus recent au plus ancien
	public function getCommentairesDuNouvAuVieu($nom) {
		$sql = 'SELECT ID_COMMENTAIRE, CONTENU, DATE_COMMENTAIRE FROM COMMENTAIRE WHERE ID_USER = (SELECT ID_USER FROM UTILISATEUR WHERE NOM_USER = ?)ORDER BY DATE_COMMENTAIRE DESC';

		$commantaires = $this->executerRequete($sql, array($nom));
		return $commantaires;
	}
	//Methode pour acceder aux commentaires du plus ancien au plus recent
	public function getCommentairesDuVieuAuNouv($nom) {

		$sql = 'SELECT ID_COMMENTAIRE, CONTENU, DATE_COMMENTAIRE FROM COMMENTAIRE WHERE ID_USER = (SELECT ID_USER FROM UTILISATEUR WHERE NOM_USER = ?)ORDER BY DATE_COMMENTAIRE ASC';

		$commantaires = $this->executerRequete($sql, array($nom));
		return $commantaires;
	}
	//Methode pour supprimer un commentaire
	public function supprimerCommentaire($idComment) {
		$sql = 'DELETE FROM COMMENTAIRE WHERE ID_COMMENTAIRE = ?';

		$resultat = $this->executerRequete($sql, array($idComment));

		$this->executerRequete($sql, array($idComment));
	}
	//Methode pour ajouter un commentaire
	public function ajouterCommentaire($nom, $contenu) {
		$sql = 'SELECT ID_USER FROM UTILISATEUR WHERE NOM_USER =?';

		$resultat = $this->executerRequete($sql, array($nom));
		$resultat = $resultat->fetch();
		$idUser   = $resultat['ID_USER'];
		$sql      = 'INSERT INTO COMMENTAIRE(CONTENU, ID_USER) values(?, ?)';

		$this->executerRequete($sql, array($contenu, $idUser));
	}
}

?>