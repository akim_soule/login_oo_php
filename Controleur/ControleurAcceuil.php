<?php
//La classe controleur Acceuil
class ControleurAcceuil {
	private $nomUser;

	//La fonction qui permet d'afficher l'acceuil
	public function gestionAcceuil($contenu) {
		if (isset($contenu)) {
			$message = $contenu;
		}
		require 'Vue/VueAcceuil.php';
	}

	//La fonction qui permet de gerer l'insertion d'un nouvel user
	public function gestionNouvel($nomEntre, $passwordEntre) {
		$this->nomUser = new Utilisateur($nomEntre);
		$message       = "";
		//echo "nouvel2" . $this->nomUser->getUser();
		//echo "2eme" . $this->nomUser->verificationSiLeUserExiste($nomEntre);
		if ($this->nomUser->verificationSiLeUserExiste($nomEntre)) {
			$message = 'Utilisateur deja existant</br>';
		} else {
			$this->nomUser->insertUser($nomEntre, $passwordEntre);
			$message = 'Bienvenu dans la base de donnees '.$nomEntre.'</br>';
		}
		$this->gestionAcceuil($message);
	}

}

?>
