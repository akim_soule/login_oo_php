<?php

/*spl_autoload_register(function ($class_name) {
require './Modele/'.$class_name.'.php';
});*/
//La classe journal
class ControleurJournal {
	private $commentaire;
	private $nomUser;

	//Le constructeur de la classe
	public function __construct() {
		$this->commentaire = new Commentaire();
	}

	//La fonction qui permet d'afficher le journal
	public function gestionJournal($nomUser, $message = null) {

		$commentairesDuVieuxAuNouv = $this->commentaire->
		getCommentairesDuVieuAuNouv($nomUser);
		$getCommentairesDuNouvAuVieu = $this->commentaire->
		getCommentairesDuNouvAuVieu($nomUser);

		require 'Vue/VueJournal.php';

	}

	//La fonction qui permet de commenter
	public function commenter($user, $contenu) {
		$this->commentaire->ajouterCommentaire($user, $contenu);
	}

	//La fonction qui permet de supprimer les commentaires
	public function gestionSuppression($idComment) {
		$this->commentaire->supprimerCommentaire($idComment);
	}

	//La fonction qui permet une connexion
	public function gestionConnexion($nomEntre, $passwordEntre) {
		$this->nomUser = new Utilisateur($nomEntre);

		if ($this->nomUser->verificationSiLeUserExiste($nomEntre)) {

			$motDePasseDeLaBaseDeDonnee = $this->nomUser->recupererMotDePasse($nomEntre);

			$estCeQueLeMotDePasseEstCorrect = password_verify($passwordEntre, $motDePasseDeLaBaseDeDonnee);

			if ($estCeQueLeMotDePasseEstCorrect) {
				session_start();
				$_SESSION['nom']  = $nomEntre;
				$_SESSION['pswd'] = $passwordEntre;
				$message          = "Bienvenu dans votre journal";

				$this->gestionJournal($_SESSION['nom']);
			} else {
				throw new Exception("Mauvais identifiant ou mot de passe");
			}
		} else {
			throw new Exception("Mauvais identifiant ou mot de passe");
		}
	}

}

?>
