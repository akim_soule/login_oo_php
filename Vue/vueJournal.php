
<?php $titre = 'Journal';
include("./Config/Configuration.php");
?>
<?php ob_start();?>
<?php
echo "<p>Bonjour, bienvenu dans le journal ".$_SESSION['nom']."</p>";
echo $_SESSION['nom'].' ! connected<br/>';

if (isset($message)) {
	echo $message;
}
?>
<form action="<?php echo $config['base']?>" method='POST'>
	<input type='submit' name='deconn' value='Deconnexion' class="btn btn-info">
</form><br/>
<form action="<?php echo $config['base']?>" method="POST" style="margin-bottom: 50px;">
	<textarea rows="4" cols="56" name="comment" required="" class="" class="form-control rounded-0"></textarea></br>
	<input type="submit" value="Ecrire" name="ecrire" class="btn btn-primary">
</form>
<form
<?php
if (isset($_POST['ordonnerDuRecentAuPluVieux']) ||
	$getCommentairesDuNouvAuVieu->rowCount() == 0 ||
	$commentairesDuVieuxAuNouv->rowCount() == 0) {
	echo "style='display: none;'";
}
?>
action="<?php echo $config['base']?>" method="POST">
<button type="submit" value="recent-vieux" name="ordonnerDuRecentAuPluVieux">Ordonner du recent au plus vieux</button>
</form>
</form>
<form
<?php
if (isset($_POST['ordonnerDuVieuxAuPluRecent']) ||
	$getCommentairesDuNouvAuVieu->rowCount() == 0 ||
	$commentairesDuVieuxAuNouv->rowCount() == 0) {
	echo "style='display: none;'";
}
?>
action="<?php echo $config['base']?>" method="POST">
<button type="submit" value="recent-vieux" name="ordonnerDuVieuxAuPluRecent">Ordonner du vieux au plus recent</button>
</form>
<table>
	<thead>
		<tr>
			<th></th>
			<th style="width: 250px;"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		if (isset($_POST['ordonnerDuRecentAuPluVieux'])) {

			foreach ($getCommentairesDuNouvAuVieu as $comment) {
				echo "<tr >
				<td style='background-color: grey; width: 100px; border-bottom: 1px solid black;'></td>
				<td style='border-bottom: 1px solid black;'>
				<div  style='margin-bottom:50px;'>
				<p>".$comment['CONTENU']."</p>
				<p style='font-size: 10px; float:left;'>Ajoute le ".$comment['DATE_COMMENTAIRE']."</p>
				<form action=".$config['base']." method = 'POST'>
				<button class='btn-danger' style='float: right;' type='submit' value=".$comment['ID_COMMENTAIRE']." name = 'supprimerCommentaire'>supprimer</button>
				</form>

				</div>
				</td>

				</tr>";
			}
		} else {
			foreach ($commentairesDuVieuxAuNouv as $comment) {
				echo "<tr >
				<td style='background-color: grey; width: 100px; border-bottom: 1px solid black;'></td>
				<td style='border-bottom: 1px solid black;'>
				<div  style='margin-bottom:50px;'>
				<p>".$comment['CONTENU']."</p>
				<p style='font-size: 10px; float:left;'>Ajoute le ".$comment['DATE_COMMENTAIRE']."</p>
				<form action=".$config['base']." method = 'POST'>
				<button class='btn-danger' style='float: right;' type='submit' value=".$comment['ID_COMMENTAIRE']." name = 'supprimerCommentaire'>supprimer</button>
				</form>

				</div>
				</td>

				</tr>";
			}
		}
		?>
	</tbody>
</table>

<?php $contenu = ob_get_clean();?>

<?php require 'vueGabarit.php';?>

